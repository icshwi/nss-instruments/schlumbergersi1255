require(stream, 2.8.8)
require(schlumbergerSi1255, "0.0.1")

epicsEnvSet(STREAM_PROTOCOL_PATH, "$(schlumbergerSi1255_DIR)db")
iocshLoad("$(schlumbergerSi1255_DIR)schlumbergerSi1255.iocsh", "PREFIX=LabS-Utgard-Schlumberger-Si1255-01, IPADDR=10.4.0.240, IPPORT=1234, SCAN=1")

